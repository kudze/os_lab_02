cmake_minimum_required(VERSION 3.13)
project(lab02)

add_definitions("-Wall" "-g")

set(CMAKE_CXX_STANDARD 20)

file(GLOB SRC_FILES src/*.cpp)
add_library(lab02_lib ${SRC_FILES})
add_executable(lab02 main.cpp)
target_link_libraries(lab02 lab02_lib)

include(FetchContent)
FetchContent_Declare(
        googletest
        URL https://github.com/google/googletest/archive/609281088cfefc76f9d0ce82e1ff6c30cc3591e5.zip
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

file(GLOB SRC_TEST_FILES test/*.cpp)
enable_testing()
add_executable(lab02_test ${SRC_TEST_FILES})
target_link_libraries(lab02_test lab02_lib gtest_main)
include(GoogleTest)
gtest_discover_tests(lab02_test)
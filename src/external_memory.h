//
// Created by kudze on 2022-03-27.
//

#ifndef LAB02_EXTERNAL_MEMORY_H
#define LAB02_EXTERNAL_MEMORY_H

#include "memory.h"

#define EXTERNAL_MEMORY_BLOCK_COUNT 256

class ExternalMemory : public MemoryInterface {
    std::array<memory_block_t, EXTERNAL_MEMORY_BLOCK_COUNT> memory;

public:
    ExternalMemory();
    virtual ~ExternalMemory();

    void writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) override;
    void writeBlock(block_address_t blockAddress, memory_block_t const& block) override;
    memory_word_t readWord(block_address_t blockAddress, word_address_t wordAddress) const override;
    memory_block_t readBlock(block_address_t blockAddress) const override;
    void print();
};


#endif //LAB02_EXTERNAL_MEMORY_H

//
// Created by kudze on 2022-03-24.
//

#ifndef LAB02_REGISTERS_H
#define LAB02_REGISTERS_H

#include <cstdint>

#include "memory.h"

template<typename T>
class Register {
    T value;

public:
    explicit Register(T value) : value(value) {};

    ~Register() = default;

    void setValue(T value) { this->value = value; };

    T getValue() const { return this->value; };
};

using Int8Register = Register<int8_t>;
using Int16Register = Register<int16_t>;
using Int32Register = Register<int32_t>;

using UInt8Register = Register<uint8_t>;
using UInt16Register = Register<uint16_t>;
using UInt32Register = Register<uint32_t>;

using BlockAddressRegister = Register<block_address_t>;

#define PTR_EXPANDED_UNUSED 0
#define PTR_EXPANDED_PAGE_TABLE_SIZE 1
#define PTR_EXPANDED_REAL_BLOCK_ADDRESS_SIZE 2
#define PTR_EXPANDED_WORD_ADDRESS_SIZE 3

struct PtrExpanded {
    uint8_t unused;
    uint8_t pageTableSize;
    uint8_t realBlockAddressSize;
    uint8_t realWordAddressSize;
};

union PtrUnion {
private:
    std::uint32_t value;
    PtrExpanded expanded;

public:
    explicit PtrUnion(uint32_t value) : value(value) {};

    PtrUnion(uint8_t unused, uint8_t pageTableSize, uint8_t realBlockAddress, uint8_t wordAddress)
            : expanded({unused, pageTableSize, realBlockAddress, wordAddress})
    {

    }

    uint32_t getValue() const { return value; }
    PtrExpanded const& getExpanded() const { return expanded; }
};

using PtrRegister = Register<PtrUnion>;

#endif //LAB02_REGISTERS_H

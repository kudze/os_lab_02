//
// Created by kudze on 2022-03-25.
//

#ifndef LAB02_CHANNEL_MANAGER_H
#define LAB02_CHANNEL_MANAGER_H

#include "registers.h"
#include "memory.h"

#define CHANNEL_OBJECT_USER_MEMORY 0
#define CHANNEL_OBJECT_SUPERVISOR_MEMORY 1
#define CHANNEL_OBJECT_INPUT_OUTPUT 2
#define CHANNEL_OBJECT_OUTSIDE_MEMORY 3

class Processor;

class ChannelManager {
    UInt8Register* sb; //Objektas iš kurio kopijuosime
    UInt8Register* db; //Objektas į kurį kopijuosime
    UInt8Register* st; //Atminties adresas iš kurio kopijuosime
    UInt8Register* dt; //Atminties adresas į kurį kopijuosime

public:
    ChannelManager();
    ~ChannelManager();

    memory_block_t readSource(Processor* processor);
    void writeDestination(Processor* processor, memory_block_t const& data);

    UInt8Register *getSb() const;
    UInt8Register *getDb() const;
    UInt8Register *getSt() const;
    UInt8Register *getDt() const;
};


#endif //LAB02_CHANNEL_MANAGER_H

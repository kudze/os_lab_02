//
// Created by kudze on 2022-03-24.
//

#ifndef LAB02_MEMORY_H
#define LAB02_MEMORY_H

#include <cstdint>
#include <array>

#include "segments.h"

#define MEMORY_BLOCK_COUNT 256          //How many blocks in memory.
#define MEMORY_BLOCK_SIZE 16            //How many words in block.
#define MEMORY_WORD_COUNT 4096              //How many total words in memory.

#define MEMORY_USER_BLOCK_OFFSET 0
#define MEMORY_USER_BLOCK_COUNT 192
#define MEMORY_SUPERVISOR_BLOCK_OFFSET MEMORY_USER_BLOCK_COUNT
#define MEMORY_SUPERVISOR_BLOCK_COUNT 64

typedef uint8_t block_address_t; // Range [0, 255]
typedef uint8_t word_address_t; // Range [0, 15]

typedef uint32_t memory_word_t;
using memory_block_t = std::array<memory_word_t, MEMORY_BLOCK_SIZE>;

class MemoryInterface {
public:
    virtual void writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) = 0;
    virtual void writeWord(uint16_t combinedAddress, memory_word_t value);
    virtual void writeBlock(block_address_t blockAddress, memory_block_t const& block) = 0;
    virtual memory_word_t readWord(block_address_t blockAddress, word_address_t wordAddress) const = 0;
    virtual memory_word_t readWord(uint16_t combinedAddress) const;
    virtual memory_block_t readBlock(block_address_t blockAddress) const = 0;
};

class Memory : public MemoryInterface {
    std::array<memory_block_t, MEMORY_BLOCK_COUNT> memory;

public:
    Memory();
    virtual ~Memory();

    void writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) override;
    void writeBlock(block_address_t blockAddress, memory_block_t const& block) override;
    memory_word_t readWord(block_address_t blockAddress, word_address_t wordAddress) const override;
    memory_block_t readBlock(block_address_t blockAddress) const override;

    void writeUserWord(block_address_t userBlockAddress, word_address_t wordAddress, memory_word_t value);
    void writeUserWord(uint16_t combinedAddress, memory_word_t value);
    void writeUserBlock(block_address_t blockAddress, memory_block_t const& block);
    memory_word_t  readUserWord(block_address_t userBlockAddress, word_address_t wordAddress) const;
    memory_word_t  readUserWord(uint16_t combinedAddress) const;
    memory_block_t readUserBlock(block_address_t userBlockAddress) const;

    void writeSupervisorWord(block_address_t supervisorBlockAddress, word_address_t wordAddress, memory_word_t value);
    void writeSupervisorWord(uint16_t combinedAddress, memory_word_t value);
    void writeSupervisorBlock(block_address_t blockAddress, memory_block_t const& block);
    memory_word_t  readSupervisorWord(block_address_t supervisorBlockAddress, word_address_t wordAddress) const;
    memory_word_t  readSupervisorWord(uint16_t combinedAddress) const;
    memory_block_t readSupervisorBlock(block_address_t supervisorBlockAddress) const;

    void printSupervisor();
    void printUser();

};


#endif //LAB02_MEMORY_H

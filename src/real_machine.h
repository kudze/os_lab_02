//
// Created by kudze on 2022-03-24.
//

#ifndef LAB02_REAL_MACHINE_H
#define LAB02_REAL_MACHINE_H

#include "processor.h"
#include "memory.h"
#include "channel_manager.h"
#include "page_mechanism.h"
#include "external_memory.h"

class RealMachine {
    bool shouldContinue;
    bool debug;

    Processor* processor;
    Memory* memory;
    ChannelManager* channelManager;
    PageMechanism* pageMechanism;
    ExternalMemory* externalMemory;

    void tick();

public:
    RealMachine(bool debug = false);
    ~RealMachine();

    void run();
    void stopOnNextTick();

    bool getShouldContinue() const;
    bool isDebug() const;
    Processor* getProcessor() const;
    Memory* getMemory() const;
    ChannelManager* getChannelManager() const;
    PageMechanism *getPageMechanism() const;

    ExternalMemory *getExternalMemory() const;
};

#endif //LAB02_REAL_MACHINE_H

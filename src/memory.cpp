//
// Created by kudze on 2022-03-24.
//

#include <iostream>
#include <iomanip>
#include "memory.h"
#include "registers.h"

Memory::Memory() {

}

Memory::~Memory() {

}

memory_block_t Memory::readBlock(block_address_t blockAddress) const {
    return this->memory.at(blockAddress);
}

memory_word_t Memory::readWord(block_address_t blockAddress, word_address_t wordAddress) const {
    return this->memory.at(blockAddress).at(wordAddress);
}

memory_word_t MemoryInterface::readWord(uint16_t combinedAddress) const {
    return this->readWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE);
}

void Memory::writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) {
    this->memory.at(blockAddress).at(wordAddress) = value;
}

void MemoryInterface::writeWord(uint16_t combinedAddress, memory_word_t value) {
    this->writeWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE, value);
}

void Memory::writeBlock(block_address_t blockAddress, const memory_block_t &block) {
    this->memory.at(blockAddress) = block;
}

memory_word_t Memory::readUserWord(block_address_t userBlockAddress, word_address_t wordAddress) const {
    return this->readWord(MEMORY_USER_BLOCK_OFFSET + userBlockAddress, wordAddress);
}

memory_word_t Memory::readUserWord(uint16_t combinedAddress) const {
    return this->readUserWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE);
}

void Memory::writeUserWord(block_address_t userBlockAddress, word_address_t wordAddress, memory_word_t value) {
    this->writeWord(MEMORY_USER_BLOCK_OFFSET + userBlockAddress, wordAddress, value);
}

void Memory::writeUserWord(uint16_t combinedAddress, memory_word_t value) {
    this->writeUserWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE, value);
}

void Memory::writeUserBlock(block_address_t blockAddress, const memory_block_t &block) {
    this->writeBlock(MEMORY_USER_BLOCK_OFFSET + blockAddress, block);
}

memory_block_t Memory::readUserBlock(block_address_t userBlockAddress) const {
    return this->readBlock(MEMORY_USER_BLOCK_OFFSET + userBlockAddress);
}

memory_word_t Memory::readSupervisorWord(block_address_t supervisorBlockAddress, word_address_t wordAddress) const {
    return this->readWord(MEMORY_SUPERVISOR_BLOCK_OFFSET + supervisorBlockAddress, wordAddress);
}

memory_word_t Memory::readSupervisorWord(uint16_t combinedAddress) const {
    return this->readSupervisorWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE);
}

memory_block_t Memory::readSupervisorBlock(block_address_t supervisorBlockAddress) const {
    return this->readBlock(MEMORY_SUPERVISOR_BLOCK_OFFSET + supervisorBlockAddress);
}

void Memory::writeSupervisorBlock(block_address_t blockAddress, const memory_block_t &block) {
    this->writeBlock(MEMORY_SUPERVISOR_BLOCK_OFFSET + blockAddress, block);
}

void Memory::writeSupervisorWord(block_address_t supervisorBlockAddress, word_address_t wordAddress, memory_word_t value) {
    this->writeWord(MEMORY_SUPERVISOR_BLOCK_OFFSET + supervisorBlockAddress, wordAddress, value);
}

void Memory::writeSupervisorWord(uint16_t combinedAddress, memory_word_t value) {
    this->writeSupervisorWord(combinedAddress / MEMORY_BLOCK_SIZE, combinedAddress % MEMORY_BLOCK_SIZE, value);
}

void Memory::printSupervisor(){

    std::cout << "Supervisor memory: "<< std::endl << "  ";

    for(int i = 0; i < MEMORY_BLOCK_SIZE; i++){
        std::cout << std::hex << std::setfill('0') << std::setw(2) << "    " << i << "    ";
    }
    std::cout << std::endl;
    for(int i = 0; i < MEMORY_SUPERVISOR_BLOCK_COUNT; i++){
        std::cout << std::hex << std::setfill('0') << std::setw(2) << i << " ";
        for(int j = 0; j < MEMORY_BLOCK_SIZE; ++j){
            std::cout << std::hex << std::setfill('0') << std::setw(8) << readSupervisorWord(i,j) << " ";
        }
        std::cout << std::endl;
    }
}
void Memory::printUser(){
    std::cout << "User memory: "<< std::endl << "  ";

    for(int i = 0; i < MEMORY_BLOCK_SIZE; i++){
        std::cout << std::hex << std::setfill('0') << std::setw(2) << "    " << i << "    ";
    }
    std::cout << std::endl;
    for(int i = 0; i < MEMORY_USER_BLOCK_COUNT; i++){
        std::cout << std::hex << std::setfill('0') << std::setw(2) << i << " ";
        for(int j = 0; j < MEMORY_BLOCK_SIZE; ++j){
            std::cout << std::hex << std::setfill('0') << std::setw(8) << readUserWord(i,j) << " ";
        }
        std::cout << std::endl;
    }
}
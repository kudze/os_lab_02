//
// Created by kudze on 2022-03-27.
//

#include <iostream>
#include <iomanip>
#include "external_memory.h"

ExternalMemory::ExternalMemory() {

}

ExternalMemory::~ExternalMemory() {
    
}

memory_block_t ExternalMemory::readBlock(block_address_t blockAddress) const {
    return this->memory.at(blockAddress);
}

memory_word_t ExternalMemory::readWord(block_address_t blockAddress, word_address_t wordAddress) const {
    return this->memory.at(blockAddress).at(wordAddress);
}

void ExternalMemory::writeWord(block_address_t blockAddress, word_address_t wordAddress, memory_word_t value) {
    this->memory.at(blockAddress).at(wordAddress) = value;
}

void ExternalMemory::writeBlock(block_address_t blockAddress, const memory_block_t &block) {
    this->memory.at(blockAddress) = block;
}
void ExternalMemory::print(){
    std::cout << "User memory: "<< std::endl << "  ";

    for(int i = 0; i < MEMORY_BLOCK_SIZE; i++){
        std::cout << std::hex << std::setfill('0') << std::setw(2) << "    " << i << "    ";
    }
    std::cout << std::endl;
    for(int i = 0; i < EXTERNAL_MEMORY_BLOCK_COUNT; i++){
        std::cout << std::hex << std::setfill('0') << std::setw(2) << i << " ";
        for(int j = 0; j < MEMORY_BLOCK_SIZE; ++j){
            std::cout << std::hex << std::setfill('0') << std::setw(8) << readWord(i,j) << " ";
        }
        std::cout << std::endl;
    }
}
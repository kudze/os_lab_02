#include "real_machine.h"

#include <iostream>

using namespace std;

RealMachine::RealMachine(bool debug) : shouldContinue(true), debug(debug) {
    this->processor = new Processor(this);
    this->memory = new Memory();
    this->externalMemory = new ExternalMemory();
    this->channelManager = new ChannelManager();
    this->pageMechanism = new PageMechanism(this->getMemory());
}

RealMachine::~RealMachine() {
    delete this->pageMechanism;
    delete this->channelManager;
    delete this->externalMemory;
    delete this->memory;
    delete this->processor;
}

void RealMachine::run() {

    while (this->getShouldContinue())
        this->tick();

}

void RealMachine::tick() {
    auto instructionType = processor->tick();

    if (this->isDebug()) {
        cout << hex << "[DEBUG] Instruction was ran of type: " << (int) instructionType
             << ", IC: " << this->getProcessor()->getIC()->getValue() << ", "
             << "MODE: " << (int) this->getProcessor()->getMODE()->getValue() << endl;

        while (true) {
            cout << "What to do now?" << endl
                 << "Possibilities: " << endl
                 << "* continue - runs next instruction" << endl
                 << "* processor - dumps processor data" << endl
                 << "* supervisor - dumps supervisor data" << endl
                 << "* user - dumps user data" << endl
                 << "* external - dumps external data" << endl << endl

                 << "Input: ";

            string input;
            cin >> input;

            if (input == "continue" || input == "c")
                break;

            if (input == "processor" || input == "p") {
                this->getProcessor()->print();
                continue;
            }

            if (input == "supervisor" || input == "s") {
                this->getMemory()->printSupervisor();
                continue;
            }

            if (input == "user" || input == "u") {
                this->getMemory()->printUser();
                continue;
            }

            if (input == "external" || input == "e") {
                this->getExternalMemory()->print();
                continue;
            }

            cout << "Unrecognized input!" << endl << endl;
        }
    }
}

void RealMachine::stopOnNextTick() {
    this->shouldContinue = false;
}

bool RealMachine::getShouldContinue() const {
    return this->shouldContinue;
}

bool RealMachine::isDebug() const {
    return this->debug;
}

Processor *RealMachine::getProcessor() const {
    return this->processor;
}

Memory *RealMachine::getMemory() const {
    return this->memory;
}

ChannelManager *RealMachine::getChannelManager() const {
    return this->channelManager;
}

PageMechanism *RealMachine::getPageMechanism() const {
    return pageMechanism;
}

ExternalMemory *RealMachine::getExternalMemory() const {
    return externalMemory;
}

#include "src/real_machine.h"

#include <iostream>
#include <cmath>
#include <cstring>

using namespace std;

void greet() {
    cout
        << "VU OS Laboratorinis 02." << endl
        << endl
        << "Credits: " << endl
        << "* Gediminas Kripas" << endl
        << "* Karolis Kraujelis" << endl << endl;
}

void writeStringToMemoryBlock(const char* message, block_address_t blockAddress, MemoryInterface* memory)
{
    auto message_len = strlen(message);
    if(message_len >= 64)
        throw std::invalid_argument("writeStringToMemoryBlock message length should be less than 64");

    auto msg_ptr = reinterpret_cast<const memory_word_t *>(message);
    auto wordsToWrite = (word_address_t) ceil(message_len / 4.f);

    for(word_address_t i = 0; i < wordsToWrite; i++)
        memory->writeWord(blockAddress, i, msg_ptr[i]);

    for(word_address_t i = wordsToWrite; i < MEMORY_BLOCK_SIZE; i++)
        memory->writeWord(blockAddress, i, 0x00000000);
}

#define OS_PROCEDURE_START_VM_OFFSET SUPERVISOR_CODE_SEGMENT_OFFSET + 1

void writeOS(RealMachine* machine)
{
    auto memory = machine->getMemory();
    writeStringToMemoryBlock("Welcome from dummy OS", MEMORY_SUPERVISOR_BLOCK_OFFSET + SUPERVISOR_DATA_SEGMENT_OFFSET, memory);
    writeStringToMemoryBlock("Which program to load? [0-9]", MEMORY_SUPERVISOR_BLOCK_OFFSET + SUPERVISOR_DATA_SEGMENT_OFFSET + 1, memory);

    //int main();
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 0, 0xF0000000); //PB [0]
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 1, 0xF0010000); //PB [1]
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 2, 0xF6020000); //GB [2]
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 3, 0xAD020000); //LW R1 [2 0]

    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 4, 0xAB010000); //LW R2
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 5, 0x30); //0x30

    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 6, 0xA1000100); //SUB R1 R2
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 7, 0xE4001000); //CALL start_vm
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 8, 0xA5000000); //INC R1
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 9, 0xE6000A00); //LOOP 10

    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 10, 0xE8000001); //SICMODE 0x00 0x01
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 11, INSTRUCTION_HALT_FULL);

    //start_vm(R1 = vmIndex);
    memory->writeSupervisorWord(OS_PROCEDURE_START_VM_OFFSET, 0, 0x00000000); //PTALLOC
    memory->writeSupervisorWord(OS_PROCEDURE_START_VM_OFFSET, 1, 0x01000000); //PTCOPY
    memory->writeSupervisorWord(OS_PROCEDURE_START_VM_OFFSET, 2, 0xE5000000); //RET

    auto processor = machine->getProcessor();
    processor->getMODE()->setValue(MODE_REGISTER_VALUE_SUPERVISOR);
    processor->getIC()->setValue(0);
}

void writeProgram(RealMachine* machine) {
    auto external_memory = machine->getExternalMemory();

    //PROGRAM 0.
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET, 0, 0xF0000000);
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET, 1, INSTRUCTION_HALT_FULL);

    writeStringToMemoryBlock("Hello world from program!", VIRTUAL_DATA_SEGMENT_OFFSET, external_memory);

    //PROGRAM 1.
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS, 0, 0xF0010000); //PB 1
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS, 1, 0xF6000000); //GB 0
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS, 2, 0xF0020000); //PB 2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS, 3, 0xF0000000); //PB 0
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS, 4, INSTRUCTION_HALT_FULL);

    writeStringToMemoryBlock("Enter input:", VIRTUAL_DATA_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS + 1, external_memory);
    writeStringToMemoryBlock("You inputted:", VIRTUAL_DATA_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS + 2, external_memory);

    //PROGRAM 2 - Example program.
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 0, 0xAB000000); //LWRC R1
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 1, 10); //10

    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 2, 0xAB010000); //LWRC R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 3, 8); //8
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 4, 0xA0000100); //ADD R1 R2

    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 5, 0xAB010000); //LWRC R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 6, 6); //6
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 7, 0xA0000100); //ADD R1 R2

    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 8, 0xAB010000); //LWRC R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 9, 4); //2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 10, 0xA0000100); //ADD R1 R2

    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 11, 0xAB010000); //LWRC R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 12, 2); //2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 13, 0xA0000100); //ADD R1 R2

    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 14, 0xAB010000); //LWRC R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, 15, 5); //2

    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 0, 0xA3000100); //DIV R1 R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 1, 0xAB010000); //LWRC R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 2, 0x30); //ASCI 0
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 3, 0xA0000100); //ADD R1 R2
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 4, 0xF0000000); //PB 0
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 5, 0xAC000100); //LWRM R1 [2 0]
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 6, 0xF0010000); //PB 1
    external_memory->writeWord(VIRTUAL_CODE_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2 + 1, 7, INSTRUCTION_HALT_FULL); //HALT

    writeStringToMemoryBlock("10 8 6 4 2 vidurkis yra: ", VIRTUAL_DATA_SEGMENT_OFFSET + VIRTUAL_MEMORY_TOTAL_BLOCKS * 2, external_memory);
}

void run() {

    cout << "Starting up..." << endl;
    cout << "Run in debug mode: ";

    bool debug = false;
    cin >> debug;

    RealMachine* real = new RealMachine(debug);

    //writeHelloWorldOS(real);
    writeOS(real);
    writeProgram(real);

    cout << "Running the machine..." << endl;
    real->run();

    delete real;
}

int main() {
    srand(time(NULL));

    greet();
    run();
}

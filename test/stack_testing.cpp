//
// Created by kudze on 2022-03-26.
//

#include <gtest/gtest.h>

#include "../src/real_machine.h"

// Demonstrate some basic assertions.
TEST(MachineTesting, StackInstructionTest) {
    auto machine = new RealMachine();

    machine->getProcessor()->getR1()->setValue(123);
    machine->getProcessor()->getR2()->setValue(234);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 123);
    ASSERT_EQ(machine->getProcessor()->getR2()->getValue(), 234);

    machine->getProcessor()->inst_ssr(REGISTER_INDEX_R1);
    machine->getProcessor()->inst_ssr(REGISTER_INDEX_R2);

    machine->getProcessor()->getR1()->setValue(1);
    machine->getProcessor()->getR2()->setValue(2);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 1);
    ASSERT_EQ(machine->getProcessor()->getR2()->getValue(), 2);

    machine->getProcessor()->inst_lsr(REGISTER_INDEX_R2);
    machine->getProcessor()->inst_lsr(REGISTER_INDEX_R1);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 123);
    ASSERT_EQ(machine->getProcessor()->getR2()->getValue(), 234);

    delete machine;
}
//
// Created by kudze on 2022-03-26.
//

#include <gtest/gtest.h>

#include "../src/real_machine.h"

// Demonstrate some basic assertions.
TEST(MachineTesting, LoadInstructionRealTesting) {
    auto machine = new RealMachine();
    auto memory = machine->getMemory();

    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 0, 0xAB000000);
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 1, 2000);
    memory->writeSupervisorWord(SUPERVISOR_CODE_SEGMENT_OFFSET, 2, INSTRUCTION_HALT_FULL);
    machine->run();

    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 2000);

    delete machine;
}
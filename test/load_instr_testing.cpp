//
// Created by kudze on 2022-03-26.
//

#include <gtest/gtest.h>

#include "../src/real_machine.h"

// Demonstrate some basic assertions.
TEST(MachineTesting, LoadInstructionTesting) {
    auto machine = new RealMachine();

    machine->getMemory()->writeSupervisorWord(SUPERVISOR_DATA_SEGMENT_OFFSET, 0, 345);
    machine->getMemory()->writeSupervisorWord(SUPERVISOR_DATA_SEGMENT_OFFSET, 1, 456);

    machine->getProcessor()->getR1()->setValue(0);
    machine->getProcessor()->getR2()->setValue(0);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 0);
    ASSERT_EQ(machine->getProcessor()->getR2()->getValue(), 0);

    machine->getProcessor()->inst_lwrc(REGISTER_INDEX_R1, 123);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 123);
    ASSERT_EQ(machine->getProcessor()->getR2()->getValue(), 0);

    machine->getProcessor()->inst_lwrr(REGISTER_INDEX_R1, REGISTER_INDEX_R2);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 123);
    ASSERT_EQ(machine->getProcessor()->getR2()->getValue(), 123);

    machine->getProcessor()->inst_lwmr(0, 0, REGISTER_INDEX_R1);
    machine->getProcessor()->inst_lwmr(0, 1, REGISTER_INDEX_R2);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 345);
    ASSERT_EQ(machine->getProcessor()->getR2()->getValue(), 456);

    machine->getProcessor()->inst_add(REGISTER_INDEX_R1, REGISTER_INDEX_R2);
    ASSERT_EQ(machine->getProcessor()->getR1()->getValue(), 345 + 456);

    machine->getProcessor()->inst_lwrm(REGISTER_INDEX_R1, 0, 0);
    ASSERT_EQ(machine->getMemory()->readSupervisorWord(SUPERVISOR_DATA_SEGMENT_OFFSET, 0), 345 + 456);

    delete machine;
}
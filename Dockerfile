FROM gcc:9.4

RUN set -ex;                       \
    apt-get update;                \
    apt-get install -y cmake;      \
    mkdir -p /usr/src;

COPY . /usr/src/lab02

RUN mkdir /usr/src/lab02/build ; \
    cd /usr/src/lab02/build ; \
    cmake .. ; \
    ctest ; \
    make

ENTRYPOINT ["/usr/src/lab02/build/lab02"]